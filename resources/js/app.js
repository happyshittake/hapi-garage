import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import { DateTime } from 'luxon'

window.Vue = Vue
Vue.use(Buefy)

Vue.filter('unixToHuman', (v) => DateTime.fromJSDate(new Date(v)).toLocaleString(DateTime.DATE_FULL))
Vue.component('garage-table', require('./components/garage-table'))
Vue.component('car-table', require('./components/car-table'))
Vue.component('form-garage', require('./components/form-garage'))
Vue.component('form-car', require('./components/form-car'))
const app = new Vue({
  el: '#app'
})