const repo = require('../app/repository/GarageRepository')
const knex = require('../config/knex')
const {Model} = require('objection')
const Garage = require('../app/models/Garage')

beforeAll(() => {
  Model.knex(knex)
})

beforeEach(async () => {
  await knex.migrate.latest()
    .then(() => knex.seed.run())
})

afterEach(async () => {
  await knex.migrate.rollback()
})

describe('Garage repository', () => {
  it('should be able get all garages with car count', async () => {
    const GARAGE_SEED_COUNT = 30

    const garages = await repo.getAllGaragesWithCarsCount()

    expect(garages).toHaveLength(GARAGE_SEED_COUNT)
    expect(garages.reduce((acc, v) => acc + v.car_count, 0)).toEqual(GARAGE_SEED_COUNT)
  })

  it('should be able to find garage by id and get the car inside it', async () => {
    const GARAGE_ID_TEST = 4

    const garage = await repo.findGarageByIDWithCars(GARAGE_ID_TEST)

    expect(garage).toBeInstanceOf(Garage)
    expect(garage.cars.length).toEqual(1)
  })

  it('should be able to insert new garage', async () => {
    let newData = {
      name: 'Garasi baru',
      address: 'jakarta',
      phone_number: '021 8823838',
      email: 'garasi@garasi.com',
      max_car: 300
    }

    const newGarage = await repo.insertGarage(newData)

    expect(newGarage.name).toEqual(newData.name)
  })

  it('should be able to update a garage', async () => {
    let newData = {
      name: 'Garasi baru',
      address: 'jakarta',
      phone_number: '021 8823838',
      email: 'garasi@garasi.com',
      max_car: 300
    }

    const newGarage = await repo.insertGarage(newData)

    newGarage.name = 'baru lagi'

    const updatedGarage = await repo.updateGarage(newGarage.id, newGarage)

    expect(updatedGarage.name).toEqual('baru lagi')
  })
})




