module.exports = require('knex')({
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: {
    filename: 'dev.sqlite3'
  }
})