module.exports = {
  name: 'session-plugin',
  version: require('../package').version,
  register: async function (server, options) {
    await server.register({
      plugin: require('yar'),
      options: {
        cookieOptions: {
          isSecure: false,
          password: ''
        }
      }
    })
  }
}