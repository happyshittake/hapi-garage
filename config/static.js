module.exports = {
  name: 'static-server',
  version: require('../package').version,
  register: async function (server, options) {
    await server.register(require('inert'))

    server.route({
      method: 'GET',
      path: '/public/{filename*}',
      handler: {
        directory: {
          path: 'static'
        }
      }
    })
  }
}