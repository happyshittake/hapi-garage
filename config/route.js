const garageController = require('../app/controller/GarageController')
const carController = require('../app/controller/CarController')
const apiCar = require('../app/controller/api/CarController')

module.exports = {
  name: 'route-plugin',
  version: require('../package').version,
  register: async (server, options) => {
    server.route({
      method: 'GET',
      path: '/',
      handler: (request, h) => h.redirect('/garages')
    })

    server.route(garageController.index)
    server.route(garageController.create)
    server.route(garageController.store)
    server.route(garageController.edit)
    server.route(garageController.update)
    server.route(garageController.show)

    server.route(carController.create)
    server.route(carController.store)
    server.route(carController.edit)
    server.route(carController.update)
    server.route(apiCar.show)

  }
}