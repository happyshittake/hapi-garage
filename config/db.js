const {Model} = require('objection')
const knex = require('./knex')

module.exports = {
  name: 'db-plugin',
  version: require('../package').version,
  register: async function (server, options) {
    Model.knex(knex)
  }
}