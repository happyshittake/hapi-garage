const Nunjucks = require('nunjucks')

module.exports = {
  name: 'view-plugin',
  version: require('../package').version,
  register: async (server, options) => {
    await server.register(require('vision'))

    server.realm.parent({
      engines: {
        html: {
          compile: (src, options) => {

            const template = Nunjucks.compile(src, options.environment)

            return (context) => {

              return template.render(context)
            }
          },

          prepare: (options, next) => {

            options.compileOptions.environment = Nunjucks.configure(options.path, {watch: false})

            return next()
          }
        }
      },
      relativeTo: __dirname,
      path: 'templates'
    })
  }
}