#Hapi-Garage

CRUD application with garage and car, using hapijs, objection orm.
Also worth mentioning it's using Jest for the testing framework.

## to start the application
- npm install
- npm run migrate-db
- npm run seed-db
- npm run asset-dev
- npm start


## todo
- more test
- config for production environment