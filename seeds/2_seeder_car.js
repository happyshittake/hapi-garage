const Car = require('../app/models/Car')
const faker = require('faker')

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex(Car.tableName).truncate()
    .then(function () {
      return knex.select('id').from('garages')
        .then(garages => {
          let seeds = []
          garages.forEach((g, i) => {
            seeds.push(
              {
                brand: faker.commerce.productName(),
                model: faker.commerce.productAdjective(),
                year: faker.date.past().getFullYear(),
                color: faker.commerce.color(),
                mileage: Math.floor(Math.random() * 3000),
                engine: faker.commerce.productMaterial(),
                power: Math.floor(Math.random() * 3000),
                registration_date: faker.date.past(),
                price: Math.floor(Math.random() * 3000),
                garage_id: g.id
              }
            )
          })

          return knex(Car.tableName).insert(seeds)
        })

    })
}
