const Garage = require('../app/models/Garage')
const faker = require('faker')

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex(Garage.tableName).truncate()
    .then(function () {
      let seeds = []
      for (let i = 1; i <= 30; i++) {
        seeds.push({
          name: faker.company.companyName(),
          address: faker.address.city(),
          phone_number: faker.phone.phoneNumber(),
          email: faker.internet.email(),
          max_car: Math.floor(Math.random() * 3000)
        })
      }

      return knex(Garage.tableName).insert(seeds)
    })
}
