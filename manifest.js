module.exports = {
  server: {
    port: 8000,
    debug: {
      request: ['*']
    }
  },
  register: {
    plugins: [
      {plugin: require('./config/db')},
      // {plugin: require('./config/view')},
      {plugin: require('./config/static')},
      // {plugin: require('./config/session')},
      {plugin: require('./config/route')}
    ]
  }
}