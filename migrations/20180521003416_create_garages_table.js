const tableName = 'garages'

exports.up = (knex, Promise) => {

  return knex.schema.hasTable(tableName).then(y => {
    if (!y) {
      return knex.schema.createTable(tableName, table => {
        table.increments('id')
        table.string('name')
        table.string('address')
        table.string('phone_number')
        table.string('email')
        table.integer('max_car')
      })
    }
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists(tableName)
}
