const tableName = 'cars'

exports.up = (knex, Promise) => {

  return knex.schema.hasTable(tableName).then(y => {
    if (!y) {
      return knex.schema.createTable(tableName, table => {
        table.increments('id')
        table.string('brand')
        table.string('model')
        table.string('year')
        table.string('color')
        table.integer('mileage').unsigned()
        table.string('engine')
        table.integer('power').unsigned()
        table.date('registration_date')
        table.decimal('price', 2, 30)

        table.integer('garage_id').unsigned()
      })
    }
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists(tableName)
}
