const Car = require('../models/Car')

exports.insertCar = async (carObj) => await Car.query().insert(carObj)

exports.findCar = async (carID) => await Car.query().findById(carID)

exports.updateCar = async (carID, carObj) => await Car.query().updateAndFetchById(carID, carObj)