const Garage = require('../models/Garage')

exports.getAllGaragesWithCarsCount = async () =>
  await Garage.query().select('id', 'name', 'address', Garage.relatedQuery('cars').count().as('car_count'))

exports.getAllGaragesForOptions = async () =>
  await Garage.query().select('id', 'name')

exports.findGarageByIDWithCars = async (garageID) =>
  await Garage.query().findById(garageID).eager('cars')

exports.insertGarage = async (garageObj) =>
  await Garage.query().insert(garageObj)

exports.updateGarage = async (id, garageObj) =>
  await Garage.query().updateAndFetchById(id, garageObj)