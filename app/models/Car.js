const {Model} = require('objection')

module.exports = class Car extends Model {
  static get relationMappings () {
    const Garage = require('./Garage')

    return {
      garage: {
        relation: Model.BelongsToOneRelation,
        modelClass: Garage,
        join: {
          from: `${Car.tableName}.garage_id`,
          to: `${Garage.tableName}.id`
        }
      }
    }
  }

  static get tableName () {
    return 'cars'
  }
}