const {Model} = require('objection')

module.exports = class Garage extends Model {
  static get tableName () {
    return 'garages'
  }

  static get relationMappings () {
    const Car = require('./Car')

    return {
      cars: {
        relation: Model.HasManyRelation,
        modelClass: Car,
        join: {
          from: `${Garage.tableName}.id`,
          to: `${Car.tableName}.garage_id`
        }
      }
    }
  }
}