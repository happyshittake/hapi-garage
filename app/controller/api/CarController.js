const repo = require('../../repository/CarRepository')
const Boom = require('boom')
const {NotFoundError} = require('objection')

exports.show = {
  method: 'GET',
  path: '/api/car/{id}',
  handler: async (request, h) => {
    let car
    try {
      car = await repo.findCar(request.params.id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.notFound('car not found')
      }

      throw Boom.internal('error')
    }

    return car
  }
}