const repo = require('../repository/GarageRepository')
const Joi = require('joi')
const {NotFoundError} = require('objection')
const Boom = require('boom')

exports.index = {
  method: 'GET',
  path: '/garages',
  handler: async (request, h) => {
    const garages = await repo.getAllGaragesWithCarsCount()

    return h.view('garages', {data: garages})
  }
}

exports.create = {
  method: 'GET',
  path: '/garage/create',
  handler: (request, h) =>
    h.view('garage-create', {
      data: {
        name: '',
        email: '',
        phone_number: '',
        max_car: 0,
        address: ''
      },
      url: '/garage/create'
    })
}

exports.store = {
  method: 'POST',
  path: '/garage/create',
  handler: async (request, h) => {
    try {
      await repo.insertGarage(request.payload)
    } catch (e) {
      throw Boom.internal('error inserting garage')
    }

    return h.redirect('/garages')
  },
  options: {
    validate: {
      payload: {
        name: Joi.string().required(),
        email: Joi.string().email().optional(),
        phone_number: Joi.string().optional(),
        max_car: Joi.number().integer().greater(0).required(),
        address: Joi.string().required()
      }
    }
  }
}

exports.edit = {
  method: 'GET',
  path: '/garage/edit/{id}',
  handler: async (request, h) => {
    let garage
    try {
      garage = await repo.findGarageByIDWithCars(request.params.id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.notFound('garage')
      }

      throw Boom.internal('error')
    }

    return h.view('garage-update', {
      data: garage,
      url: `/garage/edit/${request.params.id}`
    })
  }
}

exports.update = {
  method: 'POST',
  path: '/garage/edit/{id}',
  handler: async (request, h) => {
    try {
      await repo.updateGarage(request.params.id, request.payload)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.notFound('garage not found')
      }

      throw Boom.internal('error inserting garage')
    }

    return h.redirect('/garages')
  },
  options: {
    validate: {
      payload: {
        name: Joi.string().required(),
        email: Joi.string().email().optional(),
        phone_number: Joi.string().optional(),
        max_car: Joi.number().integer().greater(0).required(),
        address: Joi.string().required()
      },
      params: {
        id: Joi.number().integer().greater(0)
      }
    }
  }
}

exports.show = {
  method: 'GET',
  path: '/garage/{id}',
  handler: async (request, h) => {
    let garage
    try {
      garage = await repo.findGarageByIDWithCars(request.params.id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.notFound('garage')
      }

      throw Boom.internal('error')
    }

    return h.view('garage-detail', {data: garage})
  },
  options: {
    validate: {
      params: {
        id: Joi.number().integer().greater(0)
      }
    }
  }
}