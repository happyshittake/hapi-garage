const carRepo = require('../repository/CarRepository')
const garageRepo = require('../repository/GarageRepository')
const Joi = require('joi')
const Boom = require('boom')
const {NotFoundError} = require('objection')
const {DateTime} = require('luxon')

exports.create = {
  method: 'GET',
  path: '/car/create',
  handler: async (request, h) => {
    const garages = await garageRepo.getAllGaragesForOptions()

    return h.view('car-create', {
      url: '/car/create',
      data: {
        brand: '',
        model: '',
        year: new Date().getFullYear(),
        color: '',
        engine: '',
        mileage: 0,
        registration_date: new Date(),
        price: 0,
        power: 0,
        garage_id: 0
      },
      garages
    })
  }
}

exports.edit = {
  method: 'GET',
  path: '/car/edit/{id}',
  handler: async (request, h) => {
    let car
    try {
      car = await carRepo.findCar(request.params.id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.notFound('car not found')
      }

      throw Boom.internal('general error')
    }

    const garages = await garageRepo.getAllGaragesForOptions()

    return h.view('car-edit', {
      url: `/car/edit/${request.params.id}`,
      data: car,
      garages
    })
  }
}

exports.update = {
  method: 'POST',
  path: '/car/edit/{id}',
  handler: async (request, h) => {
    let garage
    try {
      garage = await garageRepo.findGarageByIDWithCars(request.payload.garage_id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.badRequest('garage id not valid')
      }
    }

    if (!(garage.max_car > garage.cars.length)) {
      throw Boom.badRequest('garage can\'t accept new car')
    }

    request.payload.registration_date = DateTime.fromFormat(request.payload.registration_date, 'M/d/yyyy').toJSDate()

    await carRepo.updateCar(request.params.id, request.payload)

    return h.redirect(`/garage/${request.payload.garage_id}`)
  }
}

exports.store = {
  method: 'POST',
  path: '/car/create',
  handler: async (request, h) => {
    let garage
    try {
      garage = await garageRepo.findGarageByIDWithCars(request.payload.garage_id)
    } catch (e) {
      if (e instanceof NotFoundError) {
        throw Boom.badRequest('garage id not valid')
      }
    }

    if (!(garage.max_car > garage.cars.length)) {
      throw Boom.badRequest('garage can\'t accept new car')
    }

    request.payload.registration_date = DateTime.fromFormat(request.payload.registration_date, 'M/d/yyyy').toJSDate()

    await carRepo.insertCar(request.payload)

    return h.redirect(`/garage/${garage.id}`)
  },
  options: {
    validate: {
      payload: {
        brand: Joi.string().required(),
        model: Joi.string().optional(),
        year: Joi.number().required(),
        color: Joi.string().optional(),
        engine: Joi.string().optional(),
        mileage: Joi.number().optional().greater(0),
        registration_date: Joi.string().required(),
        power: Joi.number().optional().greater(0),
        price: Joi.number().optional(),
        garage_id: Joi.number().greater(0).required()
      }
    }
  }
}