const Glue = require('glue')
const manifest = require('./manifest')
const Ejs = require('ejs')

async function startServer () {
  let server
  try {
    server = await Glue.compose(manifest)
    await server.register(require('vision'))

    server.views({
      engines: {ejs: Ejs},
      relativeTo: __dirname,
      path: 'templates',
      layout: true
    })

    await server.start()
    console.log(`server started on port ${manifest.server.port}`)
  } catch (e) {
    console.log(e)
    process.exit(1)
  }

}